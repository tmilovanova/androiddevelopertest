package com.example.tmilovanova.finexprototypeapp.logic;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.Loader;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;

import java.util.List;

/**
 * Loader implementation for loading exchange rates. It relies on exchange rate provider handling
 * the background threading so it can use main thread for running all its code. The loader repeatedly
 * polls the data provider with a customizable time period.
 */
public class ExchangeRateLoader extends Loader<List<ExchangeRate>> {

    private final ExchangeRateProvider provider;
    private final long repeatPeriodMillis;
    private final SimpleLogger logger;
    private final Handler handler;

    private List<ExchangeRate> cachedData;
    private boolean isLoaderRunning = false;

    /**
     * @param context the application context
     * @param provider the exchange rate provider
     * @param repeatPeriodMillis the amount of time after which the call to the provider for data
     * retrieval should be repeated
     */
    public ExchangeRateLoader(Context context, ExchangeRateProvider provider, long repeatPeriodMillis,
            SimpleLogger logger) {
        super(context);
        this.provider = provider;
        this.repeatPeriodMillis = repeatPeriodMillis;
        this.logger = logger;
        this.handler = new Handler(context.getMainLooper());
    }

    @Override
    protected void onStartLoading() {
        onForceLoad();
    }

    private void scheduleNewRun() {
        logger.debug("Scheduling a new run of the exchange rate loader in " + repeatPeriodMillis + " milliseconds...");
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                forceLoad();
            }
        }, repeatPeriodMillis);
    }

    @Override
    protected void onForceLoad() {
        // If the loader has stopped its work (due to the parent Activity or Fragment stop lifecycle event),
        // we should not allow loading data.
        if (!isStarted()) {
            return;
        }
        if (cachedData != null) {
            deliverResult(cachedData);
        }
        // If another request is running, we should not allow one more
        if (!isLoaderRunning) {
            logger.debug("Started loading exchange rates...");
            isLoaderRunning = true;
            provider.getAsync(new ExchangeRateCallback() {
                @Override
                public void onResult(final List<ExchangeRate> exchangeRate) {
                    // Since the data provider runs its heavy operations on a background thread,
                    // we should move the post-reporting onto the main thread to avoid possible
                    // synchronization issues and simplify the code. Also, the deliverResult(...) method
                    // should be called from the main thread anyway.
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            cachedData = exchangeRate;
                            deliverResult(cachedData);
                            isLoaderRunning = false;
                            logger.debug("Finished loading exchange rates.");
                            scheduleNewRun();
                        }
                    });
                }
            });
        }
    }
}
