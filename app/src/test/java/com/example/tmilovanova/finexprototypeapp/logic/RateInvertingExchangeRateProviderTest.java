package com.example.tmilovanova.finexprototypeapp.logic;

import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import org.joda.money.CurrencyUnit;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;

public class RateInvertingExchangeRateProviderTest {

    private RateInvertingExchangeRateProvider testInstance;

    @Mock
    private ExchangeRateProvider mockProvider;

    @Captor
    ArgumentCaptor<ExchangeRateCallback> callbackArgumentCaptor;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        testInstance = new RateInvertingExchangeRateProvider(mockProvider);
    }

    @Test
    public void testInvertedRatesAreAddedToTheResult() {
        List<ExchangeRate> input = new ArrayList<>(2);
        input.add(new ExchangeRate(BigDecimal.valueOf(20, 1), CurrencyUnit.EUR, CurrencyUnit.USD));
        input.add(new ExchangeRate(BigDecimal.valueOf(50, 1), CurrencyUnit.USD, CurrencyUnit.GBP));

        final List<ExchangeRate> expected = new ArrayList<>(4);
        expected.addAll(input);
        expected.add(new ExchangeRate(BigDecimal.valueOf(5, 1), CurrencyUnit.USD, CurrencyUnit.EUR));
        expected.add(new ExchangeRate(BigDecimal.valueOf(2, 1), CurrencyUnit.GBP, CurrencyUnit.USD));

        test(input, expected);
    }

    @Test
    public void testDuplicateInvertRatesAreNotAdded() {
        List<ExchangeRate> input = new ArrayList<>(2);
        input.add(new ExchangeRate(BigDecimal.valueOf(20, 1), CurrencyUnit.EUR, CurrencyUnit.USD));
        input.add(new ExchangeRate(BigDecimal.valueOf(5, 1), CurrencyUnit.USD, CurrencyUnit.EUR));

        test(input, input);
    }

    @Test
    public void testInverseRatesAreRoundedHalfUpWithScaleOfTheInitialRate() {
        List<ExchangeRate> input = new ArrayList<>(2);
        input.add(new ExchangeRate(BigDecimal.valueOf(30000, 4), CurrencyUnit.USD, CurrencyUnit.EUR));
        input.add(new ExchangeRate(BigDecimal.valueOf(12345678912367L, 2), CurrencyUnit.AUD, CurrencyUnit.EUR));

        List<ExchangeRate> expected = new ArrayList<>(4);
        expected.addAll(input);
        expected.add(new ExchangeRate(BigDecimal.valueOf(3333, 4), CurrencyUnit.EUR, CurrencyUnit.USD));
        expected.add(new ExchangeRate(BigDecimal.ZERO.scaleByPowerOfTen(-2), CurrencyUnit.EUR, CurrencyUnit.AUD));

        test(input, expected);
    }

    private void test(final List<ExchangeRate> input, final List<ExchangeRate> expected) {
        testInstance.getAsync(new ExchangeRateCallback() {
            @Override
            public void onResult(final List<ExchangeRate> exchangeRate) {
                assertTrue("These two lists contain different elements: expected " + expected + ", but was " + exchangeRate,
                        expected.containsAll(exchangeRate) && exchangeRate.containsAll(expected));
            }
        });
        verify(mockProvider).getAsync(callbackArgumentCaptor.capture());
        callbackArgumentCaptor.getValue().onResult(input);
    }
}