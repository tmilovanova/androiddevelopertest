package com.example.tmilovanova.finexprototypeapp.logic;

import lombok.NonNull;

/**
 * Interface for a exchange rate data provider retrieving the data asynchronously.
 */
public interface ExchangeRateProvider {

    void getAsync(@NonNull ExchangeRateCallback callback);
}
