package com.example.tmilovanova.finexprototypeapp.ui.model;

public enum ExchangeInputPosition {
    TOP,
    BOTTOM;

    public ExchangeDirection getCorrespondingDirection() {
        switch (this) {
            case TOP:
                return ExchangeDirection.DIRECT;
            case BOTTOM:
                return ExchangeDirection.INVERTED;
            default:
                return null;
        }
    }

    public static ExchangeInputPosition fromExchangeDirection(ExchangeDirection direction) {
        if (direction == null) {
            return null;
        }
        for (ExchangeInputPosition position : values()) {
            if (direction.equals(position.getCorrespondingDirection())) {
                return position;
            }
        }
        return null;
    }
}
