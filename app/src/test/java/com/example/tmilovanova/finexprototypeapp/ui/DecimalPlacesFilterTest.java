package com.example.tmilovanova.finexprototypeapp.ui;

import android.text.Spanned;
import org.joda.money.CurrencyUnit;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DecimalPlacesFilterTest {

    private static final String FAKE_CURRENCY_CODE_2_DIGITS = "AAA";
    private static final String FAKE_CURRENCY_CODE_3_DIGITS = "BBB";
    private DecimalPlacesFilter testInstance = new DecimalPlacesFilter(FAKE_CURRENCY_CODE_2_DIGITS);

    @BeforeClass
    public static void init() {
        CurrencyUnit.registerCurrency(FAKE_CURRENCY_CODE_2_DIGITS, 998, 2, Collections.<String>emptyList(), true);
        CurrencyUnit.registerCurrency(FAKE_CURRENCY_CODE_3_DIGITS, 997, 3, Collections.<String>emptyList(), true);
    }

    @Test
    public void testFilterDeletionWillBeAllowed() {
        final String input = "123";
        final Spanned mockSpanned = createMockSpanned(input);

        assertEquals(null, testInstance.filter("", 0, 0, mockSpanned, 0, 1));
        assertEquals(null, testInstance.filter("", 0, 0, mockSpanned, 1, 2));
        assertEquals(null, testInstance.filter("somebuffer", 2, 2, mockSpanned, 0, 2));
    }

    @Test
    public void testIntegerValuesWillBeAllowed() {
        final String input = "123";
        final String source = "456";
        final Spanned mockSpanned = createMockSpanned(input);

        assertEquals(null, testInstance.filter(source, 0, 3, mockSpanned, 0, 0));
        assertEquals(null, testInstance.filter(source, 0, 3, mockSpanned, 3, 3));
        assertEquals(null, testInstance.filter(source, 0, 3, mockSpanned, 1, 1));
        assertEquals(null, testInstance.filter(source, 0, 2, mockSpanned, 0, 2));
    }

    @Test
    public void testInsertingDecimalDigitsBelowCurrencyLimitWillBeAllowed() {
        final String input = "123.4";
        final String source = "1";
        final Spanned mockSpanned = createMockSpanned(input);

        assertEquals(null, testInstance.filter(source, 0, 1, mockSpanned, 4, 4));
        assertEquals(null, testInstance.filter(source, 0, 1, mockSpanned, 5, 5));
    }

    @Test
    public void testInsertingBeforeDecimalPointWillBeAllowed() {
        for (int i = 0; i < 4; i++) {
            assertEquals("Assert failed, i = " + i,
                    null, testInstance.filter("1", 0, 1, createMockSpanned("123.5"), i, i));
        }
        for (int i = 0; i < 4; i++) {
            assertEquals("Assert failed, i = " + i,
                    null, testInstance.filter("1", 0, 1, createMockSpanned("123.55"), i, i));
        }
    }

    @Test
    public void testInsertingDecimalDigitsAboveCurrencyLimitWillBeDisallowed() {
        final String input = "123.45";
        final String source = "1";
        final Spanned mockSpanned = createMockSpanned(input);

        assertEquals("", testInstance.filter(source, 0, 1, mockSpanned, 4, 4));
        assertEquals("", testInstance.filter(source, 0, 1, mockSpanned, 5, 5));
        assertEquals("", testInstance.filter(source, 0, 1, mockSpanned, 6, 6));
    }

    @Test
    public void testImplementationTakesDecimalPlacesCountIntoAccount() {
        DecimalPlacesFilter another = new DecimalPlacesFilter(FAKE_CURRENCY_CODE_3_DIGITS);

        final String source = "1";
        Spanned mockSpanned = createMockSpanned("123.1");
        assertEquals(null, testInstance.filter(source, 0, 1, mockSpanned, 5, 5));
        assertEquals(null, another.filter(source, 0, 1, mockSpanned, 5, 5));
        mockSpanned = createMockSpanned("123.12");
        assertEquals("", testInstance.filter(source, 0, 1, mockSpanned, 6, 6));
        assertEquals(null, another.filter(source, 0, 1, mockSpanned, 6, 6));
        mockSpanned = createMockSpanned("123.123");
        assertEquals("", testInstance.filter(source, 0, 1, mockSpanned, 7, 7));
        assertEquals("", another.filter(source, 0, 1, mockSpanned, 7, 7));
    }

    private Spanned createMockSpanned(String input) {
        Spanned result = mock(Spanned.class);
        when(result.toString()).thenReturn(input);
        when(result.length()).thenReturn(input.length());
        return result;
    }
}