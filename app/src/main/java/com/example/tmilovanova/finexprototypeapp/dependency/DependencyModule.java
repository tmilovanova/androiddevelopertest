package com.example.tmilovanova.finexprototypeapp.dependency;

import android.content.res.Resources;
import com.example.tmilovanova.finexprototypeapp.ExchangeApplication;
import com.example.tmilovanova.finexprototypeapp.logic.CompositeExchangeRateProvider;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateLoaderFactory;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateProvider;
import com.example.tmilovanova.finexprototypeapp.logic.RateInvertingExchangeRateProvider;
import com.example.tmilovanova.finexprototypeapp.logic.eurofx.EuroFxExchangeRateProvider;
import com.example.tmilovanova.finexprototypeapp.logic.eurofx.EuroFxXmlExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLoggerImpl;
import com.example.tmilovanova.finexprototypeapp.logic.openexchangerates.OERExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.openexchangerates.OERExchangeRateProvider;
import com.example.tmilovanova.finexprototypeapp.logic.openexchangerates.OERRetrievalErrorParser;
import com.example.tmilovanova.finexprototypeapp.ui.model.CurrencyAndRateUpdateModel;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;

@Module
public class DependencyModule {

    // Customizable constants that may one day belong elsewhere (in a properties file, for example)
    private static final String OER_APP_ID = "d4f30b8da4534447b70b9542624953c4";
    private static final String OER_DEFAULT_CURRENCY_BASE = "USD";
    private static final long LOADER_REPEAT_PERIOD_MILLIS = 30000;
    private static final String LOGGER_TAG = "FinExPrototypeApp";
    private static final String[] EXCHANGE_CURRENCIES = new String[] { "EUR", "GBP", "USD", };
    private static final int DEFAULT_FROM_CURRENCY_INDEX = 0;
    private static final int DEFAULT_TO_CURRENCY_INDEX = 1;

    @Provides
    public Resources applicationResources() {
        return ExchangeApplication.getContext().getResources();
    }

    @Provides
    @Named(ExchangeRateLoaderFactory.LOADER_REPEAT_PERIOD_MILLIS)
    public long getLoaderRepeatPeriodMillis() {
        return LOADER_REPEAT_PERIOD_MILLIS;
    }

    @Provides
    @Named(CurrencyAndRateUpdateModel.SUPPORTED_CURRENCIES)
    public List<String> supportedCurrencies() {
        return Arrays.asList(EXCHANGE_CURRENCIES);
    }

    @Provides
    @Named(CurrencyAndRateUpdateModel.DEFAULT_FROM_CURRENCY_INDEX)
    public int defaultFromCurrencyIndex() {
        return DEFAULT_FROM_CURRENCY_INDEX;
    }

    @Provides
    @Named(CurrencyAndRateUpdateModel.DEFAULT_TO_CURRENCY_INDEX)
    public int defaultToCurrencyIndex() {
        return DEFAULT_TO_CURRENCY_INDEX;
    }

    @Provides
    public EuroFxExchangeRateProvider euroFxExchangeRateProvider(OkHttpClient okHttpClient,
            EuroFxXmlExchangeRateParser euroFxXmlExchangeRateParser, SimpleLogger simpleLogger) {
        return new EuroFxExchangeRateProvider(okHttpClient, euroFxXmlExchangeRateParser, simpleLogger);
    }

    @Provides
    public OERExchangeRateProvider oerCurrencyRateProvider(OERExchangeRateParser oerExchangeRateParser, OERRetrievalErrorParser oerRetrievalErrorParser) {
        return new OERExchangeRateProvider(okHttpClient(), oerExchangeRateParser,
                oerRetrievalErrorParser, OER_APP_ID, OER_DEFAULT_CURRENCY_BASE, simpleLogger());
    }

    @Provides
    public ExchangeRateProvider exchangeRateProvider(CompositeExchangeRateProvider compositeExchangeRateProvider) {
        return new RateInvertingExchangeRateProvider(compositeExchangeRateProvider);
    }

    @Provides
    public CompositeExchangeRateProvider compositeExchangeRateProvider(EuroFxExchangeRateProvider euroFxExchangeRateProvider,
            OERExchangeRateProvider oerExchangeRateProvider) {
        return new CompositeExchangeRateProvider(euroFxExchangeRateProvider, oerExchangeRateProvider);
    }

    @Provides
    @Singleton
    public OkHttpClient okHttpClient() {
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    public SimpleLogger simpleLogger() {
        return new SimpleLoggerImpl(LOGGER_TAG);
    }
}