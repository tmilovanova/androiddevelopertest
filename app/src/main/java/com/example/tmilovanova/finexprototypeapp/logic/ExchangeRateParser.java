package com.example.tmilovanova.finexprototypeapp.logic;

import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;

import java.io.InputStream;
import java.util.List;

/**
 * Interface for an exchange rate parser from some raw data structure.
 */
public interface ExchangeRateParser {
    List<ExchangeRate> parse(InputStream inputStream) throws ExchangeRateParserException;
}
