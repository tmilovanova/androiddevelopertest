package com.example.tmilovanova.finexprototypeapp.ui.model;

enum ExchangeDirection {
    DIRECT,
    INVERTED;

    public ExchangeDirection flip() {
        return this == DIRECT ? INVERTED : DIRECT;
    }
}
