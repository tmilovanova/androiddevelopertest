package com.example.tmilovanova.finexprototypeapp.logic.openexchangerates;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 * Taken from https://docs.openexchangerates.org/v0.7/docs/errors .
 */
@NoArgsConstructor
@AllArgsConstructor
class OERErrorResponse {

    @Getter
    @Setter
    private boolean error;

    @Getter
    @Setter
    private int status;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private String description;
}
