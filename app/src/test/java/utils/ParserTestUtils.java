package utils;

import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParserException;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;

import java.io.InputStream;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

public class ParserTestUtils {

    public static void test(String inputFile, List<ExchangeRate> expected, ExchangeRateParser parser) {
        InputStream testInput = TestUtils.getTestResource(ParserTestUtils.class, inputFile);
        try {
            List<ExchangeRate> result = parser.parse(testInput);
            assertEquals(expected, result);
        } catch (ExchangeRateParserException ex) {
            fail("Error parsing file " + inputFile + "\n" + ex.getMessage());
        }
    }

}
