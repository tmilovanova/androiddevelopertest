package com.example.tmilovanova.finexprototypeapp.logic.logging;

import android.util.Log;

public class SimpleLoggerImpl implements SimpleLogger {

    private final String tag;

    public SimpleLoggerImpl(String tag) {
        this.tag = tag;
    }

    @Override
    public void debug(final String message) {
        Log.d(tag, message);
    }

    @Override
    public void info(final String message) {
        Log.i(tag, message);
    }

    @Override
    public void warn(final String message) {
        Log.w(tag, message);
    }

    @Override
    public void error(final String message) {
        Log.e(tag, message);
    }

    @Override
    public void error(final String message, final Exception exception) {
        Log.e(tag, message, exception);
    }
}
