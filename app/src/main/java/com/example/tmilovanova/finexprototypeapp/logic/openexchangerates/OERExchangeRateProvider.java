package com.example.tmilovanova.finexprototypeapp.logic.openexchangerates;

import com.example.tmilovanova.finexprototypeapp.logic.AbstractHttpExchangeRateProvider;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

public class OERExchangeRateProvider extends AbstractHttpExchangeRateProvider {

    private static final String OER_URL_TEMPLATE = "https://openexchangerates.org/api/latest.json?app_id=%s&base=%s";

    private final String url;
    private final OERRetrievalErrorParser errorParser;

    /**
     * @see AbstractHttpExchangeRateProvider#AbstractHttpExchangeRateProvider(OkHttpClient, ExchangeRateParser, SimpleLogger)
     * @param errorParser the parser instance to read the response in case of an HTTP error
     * @param appId the application ID to use with the Openexchangerates API
     * @param base the currency code of the base currency to request rates against
     * @see <a href="Openexchangerates official site">https://openexchangerates.org/</a>
     */
    @Inject
    public OERExchangeRateProvider(OkHttpClient httpClient, ExchangeRateParser exchangeRateParser,
            OERRetrievalErrorParser errorParser, String appId, String base, SimpleLogger logger) {
        super(httpClient, exchangeRateParser, logger);
        this.errorParser = errorParser;
        this.url = String.format(OER_URL_TEMPLATE, appId, base);
    }

    @Override
    protected List<ExchangeRate> postProcessRates(final List<ExchangeRate> parsedRates) {
        Iterator<ExchangeRate> iterator = parsedRates.iterator();
        while (iterator.hasNext()) {
            ExchangeRate rate = iterator.next();
            if (rate.getFrom().getCode().equals(rate.getTo().getCode())) {
                iterator.remove();
            }
        }
        return parsedRates;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void logHttpCallFailure(final Response response) {
        try {
            OERErrorResponse errorResponse = errorParser.parse(response.body().byteStream());
            getLogger().error(getRetrievalErrorMessage(response.code(), errorResponse.getDescription()));
        } catch (Exception ex) {
            super.logHttpCallFailure(response);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getUrl() {
        return url;
    }
}
