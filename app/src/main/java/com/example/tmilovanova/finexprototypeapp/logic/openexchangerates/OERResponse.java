package com.example.tmilovanova.finexprototypeapp.logic.openexchangerates;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/*
 * See https://docs.openexchangerates.org/docs/latest-json .
 */
@NoArgsConstructor
@AllArgsConstructor
class OERResponse {

    @Getter
    @Setter
    private String disclaimer;

    @Getter
    @Setter
    private String license;

    @Getter
    @Setter
    private Date timestamp;

    @Getter
    @Setter
    private String base;

    @Getter
    @Setter
    private Map<String, BigDecimal> rates;
}
