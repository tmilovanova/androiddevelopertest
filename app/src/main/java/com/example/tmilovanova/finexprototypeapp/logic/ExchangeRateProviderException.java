package com.example.tmilovanova.finexprototypeapp.logic;

/**
 * A generic exception wrapper for {@link ExchangeRateProvider} class.
 */
public class ExchangeRateProviderException extends Exception {
    public ExchangeRateProviderException(String message) {
        super(message);
    }

    public ExchangeRateProviderException(String message, Exception cause) {
        super(message, cause);
    }
}
