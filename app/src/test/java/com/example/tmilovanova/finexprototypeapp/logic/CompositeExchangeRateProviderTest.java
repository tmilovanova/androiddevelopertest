package com.example.tmilovanova.finexprototypeapp.logic;

import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import org.joda.money.CurrencyUnit;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CompositeExchangeRateProviderTest {

    private static final int MOCK_PROVIDER_COUNT = 3;

    private static final List<ExchangeRate> testData = new ArrayList<>(MOCK_PROVIDER_COUNT);

    private CompositeExchangeRateProvider testInstance;
    private ExchangeRateProvider[] mockProviders = new ExchangeRateProvider[MOCK_PROVIDER_COUNT];

    @BeforeClass
    public static void initClass() {
        prepareTestData();
    }

    @Before
    public void init() {
        for (int i = 0; i < MOCK_PROVIDER_COUNT; i++) {
            ExchangeRateProvider provider = mock(ExchangeRateProvider.class);
            mockProviders[i] = provider;
        }
        testInstance = new CompositeExchangeRateProvider(mockProviders);
    }

    @Test
    public void testAllProvidersAreQueriedAndResultsAreCombined() {
        testInstance.getAsync(new ExchangeRateCallback() {
            @Override
            public void onResult(final List<ExchangeRate> exchangeRate) {
                assertEquals(testData, exchangeRate);
            }
        });
        int index = 0;
        for (ExchangeRateProvider provider: mockProviders) {
            ArgumentCaptor<ExchangeRateCallback> callbackArgumentCaptor = ArgumentCaptor.forClass(ExchangeRateCallback.class);
            verify(provider).getAsync(callbackArgumentCaptor.capture());
            callbackArgumentCaptor.getValue().onResult(Collections.singletonList(testData.get(index++)));
        }
    }

    private static void prepareTestData() {
        final BigDecimal baseRate = BigDecimal.ONE.scaleByPowerOfTen(3);
        for (int i = 0; i < MOCK_PROVIDER_COUNT; i++) {
            testData.add(new ExchangeRate(baseRate.multiply(BigDecimal.valueOf(i)), CurrencyUnit.USD, CurrencyUnit.EUR));
        }
    }
}