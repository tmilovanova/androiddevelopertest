package com.example.tmilovanova.finexprototypeapp.ui;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import com.example.tmilovanova.finexprototypeapp.ui.model.CurrencyAndRateUpdateModel;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.math.BigDecimal;

/**
 * This class handles updates to EditText for money amount, both updates from user input
 * and programmatic ones. When user makes an input, the new value is published to the model.
 * Model's reaction is stopped from propagating back in the handler to avoid an infinite cycle
 * of calls.
 * Also this class handles incorrect inputs - if the input value cannot be parsed it's replaced
 * with "0". If the initial value was "0" and user inputs a new digit, the new digit replaces the "0"
 * to provide a better user experience.
 */
class ExchangeRateUpdateHandler implements TextWatcher {

    private final String ZERO_VALUE = "0";

    private final EditText amountEditText;
    private final CurrencyAndRateUpdateModel model;
    private final String currency;
    private String oldValue;
    private boolean updatesDisabled = false;

    ExchangeRateUpdateHandler(EditText amountEditText, CurrencyAndRateUpdateModel model, String currency) {
        this.amountEditText = amountEditText;
        this.model = model;
        this.currency = currency;
    }

    void setValueWithoutReaction(String value) {
        if (updatesDisabled) {
            return;
        }
        updatesDisabled = true;
        amountEditText.setText(value);
        updatesDisabled = false;
    }

    @Override
    public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
        oldValue = s.toString();
    }

    @Override
    public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
    }

    @Override
    public void afterTextChanged(final Editable s) {
        if (updatesDisabled) {
            return;
        }
        updatesDisabled = true;
        removeZeroesIfWasZero(oldValue, s);
        BigDecimal resultValue = ensureCanBeParsed(s);
        model.publishNewValue(Money.of(CurrencyUnit.of(currency), resultValue));
        updatesDisabled = false;
    }

    void removeZeroesIfWasZero(final String oldValue, final Editable s) {
        if (oldValue.equals(ZERO_VALUE) && !s.toString().equals("0.")) {
            String newValue = s.toString().replace(ZERO_VALUE, "");
            newValue = newValue.isEmpty() ? ZERO_VALUE : newValue;
            s.clear();
            s.append(newValue);
        }
    }

    BigDecimal ensureCanBeParsed(final Editable s) {
        try {
            return new BigDecimal(s.toString());
        } catch (Exception ex) {
            s.clear();
            s.append(ZERO_VALUE);
            return BigDecimal.ZERO;
        }
    }
}