package com.example.tmilovanova.finexprototypeapp.ui;

import android.text.Editable;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ExchangeRateUpdateHandlerTest {

    private ExchangeRateUpdateHandler testInstance = new ExchangeRateUpdateHandler(null, null, null);

    @Test
    public void testRemoveZeroesIfWasZero() {
        testRemoveZeroesIfWasZero("0", "05", "5", true);
        testRemoveZeroesIfWasZero("0", "50", "5", true);
        testRemoveZeroesIfWasZero("0.", "0.5", "0.5", false);
        testRemoveZeroesIfWasZero("50", "500", "500", false);
        testRemoveZeroesIfWasZero("5", "05", "05", false);
        testRemoveZeroesIfWasZero("5", "50", "50", false);
    }

    @Test
    public void testEnsureCanBeParsed() {
        testEnsureCanBeParsed("123", "123", false);
        testEnsureCanBeParsed("123.", "123.", false);
        testEnsureCanBeParsed(".56", ".56", false);
        testEnsureCanBeParsed("", "0", true);
        testEnsureCanBeParsed(".", "0", true);
        testEnsureCanBeParsed("dfgdg", "0", true);
    }

    private void testRemoveZeroesIfWasZero(String oldInput, String newInput, String expected, boolean shouldModify) {
        Editable toTest = mock(Editable.class);
        when(toTest.toString()).thenReturn(newInput);
        testInstance.removeZeroesIfWasZero(oldInput, toTest);

        if (shouldModify) {
            verify(toTest).clear();
            ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
            verify(toTest).append(valueCaptor.capture());
            assertEquals(expected, valueCaptor.getValue());
        } else {
            verify(toTest, never()).clear();
            verify(toTest, never()).append(anyString());
        }
    }

    private void testEnsureCanBeParsed(String newInput, String expected, boolean shouldModify) {
        Editable toTest = mock(Editable.class);
        when(toTest.toString()).thenReturn(newInput);
        BigDecimal result = testInstance.ensureCanBeParsed(toTest);

        if (shouldModify) {
            verify(toTest).clear();
            ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
            verify(toTest).append(valueCaptor.capture());
            assertEquals(expected, valueCaptor.getValue());
            assertEquals(BigDecimal.ZERO, result);
        } else {
            verify(toTest, never()).clear();
            verify(toTest, never()).append(anyString());
            assertEquals(new BigDecimal(expected), result);
        }
    }
}