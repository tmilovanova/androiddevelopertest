package com.example.tmilovanova.finexprototypeapp.ui;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.antonyt.infiniteviewpager.InfiniteViewPager;
import com.example.tmilovanova.finexprototypeapp.ExchangeApplication;
import com.example.tmilovanova.finexprototypeapp.R;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateLoaderFactory;
import com.example.tmilovanova.finexprototypeapp.model.Account;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import com.example.tmilovanova.finexprototypeapp.ui.model.CurrencyAndRateUpdateModel;
import com.example.tmilovanova.finexprototypeapp.ui.model.DisplayedRatesUpdateEventListener;
import com.example.tmilovanova.finexprototypeapp.ui.model.ExchangeInputPosition;
import com.viewpagerindicator.CirclePageIndicator;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.List;

public class ExchangeActivity extends AppCompatActivity
        implements DisplayedRatesUpdateEventListener,
        ExchangeViewFragment.ExchangePageModelProvider {

    @Inject
    ExchangeRateLoaderFactory exchangeRateLoaderFactory;

    @Inject
    Provider<Account> accountProvider;

    @Inject
    CurrencyAndRateUpdateModel currencyAndRateUpdateModel;

    private TextView currentRateTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ExchangeApplication.getComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);

        initializeCurrencyViewPagers();

        currentRateTextView = (TextView) findViewById(R.id.text_view_current_rate);

        currencyAndRateUpdateModel.addDisplayedRatesUpdateEventListener(this);

        ImageButton navigateBackButton = (ImageButton) findViewById(R.id.image_button_back);
        navigateBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onBackPressed();
            }
        });

        ImageButton performExchangeButton = (ImageButton) findViewById(R.id.image_button_done);
        performExchangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Toast.makeText(ExchangeActivity.this,
                        getResources().getString(R.string.perform_exchange_toast), Toast.LENGTH_SHORT).show();
            }
        });

        getSupportLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<List<ExchangeRate>>() {
            @Override
            public Loader<List<ExchangeRate>> onCreateLoader(final int id, final Bundle args) {
                return exchangeRateLoaderFactory.createLoader(ExchangeActivity.this);
            }

            @Override
            public void onLoadFinished(final Loader<List<ExchangeRate>> loader, final List<ExchangeRate> data) {
                currencyAndRateUpdateModel.onExchangeRatesUpdate(data);
            }

            @Override
            public void onLoaderReset(final Loader<List<ExchangeRate>> loader) {

            }
        });
    }

    private void initializeCurrencyViewPagers() {
        InfiniteViewPager topPager = (InfiniteViewPager) findViewById(R.id.view_pager_top_strip);
        InfiniteViewPager bottomPager = (InfiniteViewPager) findViewById(R.id.view_page_bottom_strip);
        final ExchangeViewPagerAdapter topPagerAdapter = newExchangeViewPagerAdapter(ExchangeInputPosition.TOP);

        topPager.setAdapter(new InfinitePageAdapterFixed(topPager, topPagerAdapter));
        final ExchangeViewPagerAdapter toPagerAdapter = newExchangeViewPagerAdapter(ExchangeInputPosition.BOTTOM);
        bottomPager.setAdapter(new InfinitePageAdapterFixed(bottomPager, toPagerAdapter));
        topPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(final int position) {
                currencyAndRateUpdateModel.onCurrencyUpdate(
                        topPagerAdapter.getCurrencyByPosition(position % topPagerAdapter.getCount()),
                        ExchangeInputPosition.TOP);
            }

            @Override
            public void onPageScrollStateChanged(final int state) {
            }
        });
        bottomPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(final int position) {
                currencyAndRateUpdateModel.onCurrencyUpdate(
                        toPagerAdapter.getCurrencyByPosition(position % toPagerAdapter.getCount()),
                        ExchangeInputPosition.BOTTOM);
            }

            @Override
            public void onPageScrollStateChanged(final int state) {
            }
        });

        topPager.setCurrentItem(currencyAndRateUpdateModel.getDefaultFromCurrencyIndex());
        bottomPager.setCurrentItem(currencyAndRateUpdateModel.getDefaultToCurrencyIndex());

        CirclePageIndicator topPageIndicator = (CirclePageIndicator) findViewById(R.id.view_pager_indicator_top);
        topPageIndicator.setViewPager(topPager);

        CirclePageIndicator bottomPageIndicator = (CirclePageIndicator) findViewById(R.id.view_pager_indicator_bottom);
        bottomPageIndicator.setViewPager(bottomPager);
    }

    private ExchangeViewPagerAdapter newExchangeViewPagerAdapter(ExchangeInputPosition exchangeInputPosition) {
        return new ExchangeViewPagerAdapter(getSupportFragmentManager(),
                currencyAndRateUpdateModel.getSupportedCurrencies(), accountProvider, exchangeInputPosition);
    }

    @Override
    public void onDisplayedRateRefreshed(final String newRateToDisplay) {
        currentRateTextView.setText(newRateToDisplay);
    }

    @Override
    public CurrencyAndRateUpdateModel getModel() {
        return currencyAndRateUpdateModel;
    }
}
