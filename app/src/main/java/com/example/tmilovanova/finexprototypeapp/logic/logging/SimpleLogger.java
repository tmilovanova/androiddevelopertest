package com.example.tmilovanova.finexprototypeapp.logic.logging;

public interface SimpleLogger {

    void debug(String message);

    void info(String message);

    void warn(String message);

    void error(String message);

    void error(String message, Exception exception);
}
