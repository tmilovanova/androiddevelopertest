package utils;

import java.io.InputStream;

public class TestUtils {

    private TestUtils() {}

    public static InputStream getTestResource(Class<?> clazz, String fileName) {
        return clazz.getClassLoader().getResourceAsStream(fileName);
    }
}
