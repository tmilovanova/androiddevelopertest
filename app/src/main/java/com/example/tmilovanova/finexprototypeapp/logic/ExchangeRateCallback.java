package com.example.tmilovanova.finexprototypeapp.logic;

import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;

import java.util.List;

/**
 * Interface for receiving the retrieved exchange rate info as a result of the work
 * of an {@link ExchangeRateProvider#getAsync(ExchangeRateCallback)} call.
 */
public interface ExchangeRateCallback {
    void onResult(List<ExchangeRate> exchangeRate);
}
