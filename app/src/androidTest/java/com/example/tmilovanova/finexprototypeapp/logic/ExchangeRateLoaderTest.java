package com.example.tmilovanova.finexprototypeapp.logic;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLoggerImpl;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import lombok.NonNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RunWith(AndroidJUnit4.class)
public class ExchangeRateLoaderTest extends SupportLoaderTestCase {

    @Test
    public void testExchangeRateLoader() throws Exception {
        final List<ExchangeRate> expectedData = new ArrayList<>();
        final AtomicInteger counter = new AtomicInteger(0);
        ExchangeRateProvider provider = createTestProvider(expectedData, counter);
        ExchangeRateLoader loader = new ExchangeRateLoader(
                InstrumentationRegistry.getTargetContext(), provider, 100, new SimpleLoggerImpl("AndroidTestCase"));

        List<ExchangeRate> actual = getLoaderResultSynchronously(loader);
        // We should wait some time to make sure that the ExchangeRateLoader
        // does not schedule consequent data reloads after it has been reset.
        Thread.sleep(1000);

        assertSame(expectedData, actual);
        assertEquals(1, counter.get());
    }

    private ExchangeRateProvider createTestProvider(final List<ExchangeRate> expectedData, final AtomicInteger counter) {
        return new ExchangeRateProvider() {
            @Override
            public void getAsync(@NonNull final ExchangeRateCallback callback) {
                counter.incrementAndGet();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        simulateDataLoading(callback, expectedData);
                    }
                }).start();
            }
        };
    }

    private void simulateDataLoading(final @NonNull ExchangeRateCallback callback, final List<ExchangeRate> expectedData) {
        try {
            Thread.sleep(100);
            callback.onResult(expectedData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

