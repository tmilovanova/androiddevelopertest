package com.example.tmilovanova.finexprototypeapp.logic;

import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import lombok.NonNull;
import org.joda.money.CurrencyUnit;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * An {@link ExchangeRateProvider} implementation that adds inverted rates to the rates
 * obtained from another provider.
 */
public class RateInvertingExchangeRateProvider implements ExchangeRateProvider {

    private final ExchangeRateProvider exchangeRateProvider;

    @Inject
    public RateInvertingExchangeRateProvider(ExchangeRateProvider exchangeRateProvider) {
        this.exchangeRateProvider = exchangeRateProvider;
    }

    @Override
    public void getAsync(@NonNull final ExchangeRateCallback callback) {
        exchangeRateProvider.getAsync(new ExchangeRateCallback() {
            @Override
            public void onResult(final List<ExchangeRate> exchangeRate) {
                callback.onResult(addInvertedRates(exchangeRate));
            }
        });
    }

    /*
     * Adds inverted rates to the provided list of rates.
     * Example: if the list contains the EUR->USD rate, then the method will add the USD->EUR
     * rates calculating it from the provided rate with the help of {@link ExchangeRate#inverted()} method.
     * If the initial list already contains an inverted rate, then such a rate will not be duplicated and
     * the initial instance will be preserved.
     * The initial list will not be modified, instead a new list is returned. All rates from the initial list will be there
     * and the appropriate inverted rates will be added.
     */
    private List<ExchangeRate> addInvertedRates(final List<ExchangeRate> exchangeRate) {
        List<ExchangeRate> inverted = new ArrayList<>(exchangeRate.size() * 2);
        Set<String> currencyPairs = new HashSet<>();
        for (ExchangeRate rate: exchangeRate) {
            currencyPairs.add(toCurrencyPair(rate.getFrom(), rate.getTo()));
        }
        for (ExchangeRate rate: exchangeRate) {
            String invertedPair = toCurrencyPair(rate.getTo(), rate.getFrom());
            if (!currencyPairs.contains(invertedPair)) {
                inverted.add(rate.inverted());
                currencyPairs.add(invertedPair);
            }
        }
        inverted.addAll(exchangeRate);
        return inverted;
    }

    private String toCurrencyPair(CurrencyUnit from, CurrencyUnit to) {
        return from + "->" + to;
    }
}
