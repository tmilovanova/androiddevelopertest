package com.example.tmilovanova.finexprototypeapp.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.example.tmilovanova.finexprototypeapp.R;
import com.example.tmilovanova.finexprototypeapp.ui.model.CurrencyAndRateUpdateModel;
import com.example.tmilovanova.finexprototypeapp.ui.model.ExchangeInputPosition;
import org.joda.money.Money;

import java.math.BigDecimal;
import java.util.Arrays;

public class ExchangeViewFragment extends Fragment implements CurrencyAndRateUpdateModel.ValueChangeListener {

    private static final String ARGUMENT_EXCHANGE_POSITION = "ExchangePosition";
    private static final String ARGUMENT_CURRENCY = "Currency";
    private static final String ARGUMENT_CURRENT_BALANCE = "CurrentBalance";

    private CurrencyAndRateUpdateModel model;
    private ExchangeRateUpdateHandler updateHandler;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_pager_exchange_currency_view, container, false);
        Bundle arguments = getArguments();
        if (arguments != null) {
            initializeView(view, arguments);
        }
        return view;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        try {
            model = ((ExchangePageModelProvider) getActivity()).getModel();
            model.addValueChangeListener(this);
        } catch (ClassCastException ex) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        model.removeValueChangeListener(this);
    }

    private void initializeView(final View view, final Bundle arguments) {
        TextView currencyCodeTextView = (TextView) view.findViewById(R.id.text_view_currency_code);
        if (currencyCodeTextView != null) {
            currencyCodeTextView.setText(arguments.getString(ARGUMENT_CURRENCY));
        }

        TextView balanceTextView = (TextView) view.findViewById(R.id.text_view_current_balance);
        if (balanceTextView != null) {
            balanceTextView.setText(getResources().getString(R.string.current_balance,
                    arguments.getString(ARGUMENT_CURRENT_BALANCE)));
        }

        EditText amountEditText = (EditText) view.findViewById(R.id.edit_text_amount);
        if (amountEditText != null) {
            amountEditText.setText("0");
            amountEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(final View v, final boolean hasFocus) {
                    if (hasFocus && model != null) {
                        model.onRateInputFocusObtained(
                                (ExchangeInputPosition) arguments.getSerializable(ARGUMENT_EXCHANGE_POSITION));
                    }
                }
            });
            updateHandler = new ExchangeRateUpdateHandler(amountEditText, model, getCurrency());
            amountEditText.addTextChangedListener(updateHandler);
            InputFilter[] newFilters = Arrays.copyOf(amountEditText.getFilters(), amountEditText.getFilters().length + 1);
            newFilters[newFilters.length - 1] = new DecimalPlacesFilter(getCurrency());
            amountEditText.setFilters(newFilters);
        }
    }

    private String getCurrency() {
        return getArguments().getString(ARGUMENT_CURRENCY);
    }

    private ExchangeInputPosition getFragmentPosition() {
        return (ExchangeInputPosition) getArguments().get(ARGUMENT_EXCHANGE_POSITION);
    }

    public static ExchangeViewFragment newInstance(ExchangeInputPosition exchangeDirection,
            String currencyCode, String currentBalance) {
        ExchangeViewFragment newFragment = new ExchangeViewFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(ARGUMENT_EXCHANGE_POSITION, exchangeDirection);
        arguments.putString(ARGUMENT_CURRENCY, currencyCode);
        arguments.putString(ARGUMENT_CURRENT_BALANCE, currentBalance);
        newFragment.setArguments(arguments);
        return newFragment;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void handleNewValue(Money money, ExchangeInputPosition position) {
        String fragmentCurrency = getCurrency();
        if (fragmentCurrency.equals(money.getCurrencyUnit().getCode()) && getFragmentPosition() == position) {
            BigDecimal amount = money.getAmount();
            String newValue = amount.compareTo(BigDecimal.ZERO) == 0 ? "0" : amount.toString();
            updateHandler.setValueWithoutReaction(newValue);
        } else {
            updateHandler.setValueWithoutReaction("0");
        }
    }

    public interface ExchangePageModelProvider {

        CurrencyAndRateUpdateModel getModel();
    }

}
