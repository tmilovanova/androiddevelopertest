# README #

My implementation of the Android Developer Task. 

##Implementation notes:##
1. I've taken the exchange rates from 2 sources, specified in the document previously: European Central bank site and OpenExchangeRates site and combined them together to make a full set.
2. I used Android Loader from the v4 support library as a mechanism of providing the background data to the UI.

###Third-party libraries used:###
* [Dagger2](https://google.github.io/dagger/)
* [okhttp](http://square.github.io/okhttp/)
* [Mockito](https://github.com/mockito/mockito)
* [Joda Money](https://github.com/JodaOrg/joda-money)
* [Jackson](https://github.com/FasterXML/jackson)
* [Jack Wharton's ViewPageIndicator](https://github.com/JakeWharton/ViewPagerIndicator)
* [antonyt's InfinitePageViewer](https://github.com/antonyt/InfiniteViewPager)

##"What is not perfect" notes:##

1. It is covered with Unit tests to some extent, however not fully. I would also add tests for UI logic, as well as some Android UI tests (for example, Esspresso ones), if time permitted.
2. The example UI is not mimicked to the letter (some secondary text views, a divider between two ViewPagers are not included). Also, the style of the activity only resembles the initial style, not mimicks it, but this is permitted by the task conditions (for example, I use plain EditText, while the original application clearly extends it to perform custom font changes.
3. There are some "hacks" related to the UI solutions I had to take due to the time limit. I used ready libraries to implement the looping (infinite) ViewPager and scroll indicators. Both of them require some fixes to work under restraints of the application in question, but implementing them seems to be out of scope for the task, so I put workarounds and comments explaining them.
4. [UPDATE] I have added the icons for the Back/Done buttons. I have put them to drawable/ only, not to drawable-*dpi to support different screens.
Back icon image created by [freepic](http://www.flaticon.com/authors/freepik).
Done icon image created by [madebyoliver](http://www.flaticon.com/authors/madebyoliver).