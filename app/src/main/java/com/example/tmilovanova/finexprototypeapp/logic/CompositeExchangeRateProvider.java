package com.example.tmilovanova.finexprototypeapp.logic;

import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An {@link ExchangeRateProvider} implementation that combines the exchange rate results
 * from multiple providers of any type.
 */
public class CompositeExchangeRateProvider implements ExchangeRateProvider {

    private final ExchangeRateProvider[] providers;

    public CompositeExchangeRateProvider(ExchangeRateProvider... providers) {
        this.providers = providers;
    }

    @Override
    public void getAsync(@NonNull final ExchangeRateCallback callback) {
        final AtomicInteger counter = new AtomicInteger(providers.length);
        final List<ExchangeRate> exchangeRates = new ArrayList<>();
        ExchangeRateCallback internalCallback = new ExchangeRateCallback() {
            @Override
            public void onResult(final List<ExchangeRate> exchangeRate) {
                exchangeRates.addAll(exchangeRate);
                if (counter.decrementAndGet() == 0) {
                    callback.onResult(exchangeRates);
                }
            }
        };
        for (ExchangeRateProvider provider: providers) {
            provider.getAsync(internalCallback);
        }
    }
}
