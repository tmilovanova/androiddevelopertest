package com.example.tmilovanova.finexprototypeapp.logic.openexchangerates;

import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParserException;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.money.CurrencyUnit;
import org.joda.money.IllegalCurrencyException;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class OERExchangeRateParser implements ExchangeRateParser {

    private final ObjectMapper objectMapper;
    private final SimpleLogger logger;

    @Inject
    public OERExchangeRateParser(SimpleLogger logger) {
        this.objectMapper = new ObjectMapper();
        this.logger = logger;
    }

    /**
     * Parses a JSON input stream to populate a list of exchange rates.
     * This implementation depends on the {@link OERResponse} object mapping.
     * The potential rate will be ignored if {@link OERResponse#getRates()} or
     * {@link OERResponse#getBase()} are empty.
     *
     * @param inputStream the JSON byte input stream to be parsed
     * @return the result list of the exchange rates
     * @throws ExchangeRateParserException on any exceptions thrown by the internal parser,
     * the offending exception will be included as the cause.
     */
    @Override
    public List<ExchangeRate> parse(final InputStream inputStream) throws ExchangeRateParserException {
        List<ExchangeRate> result = new ArrayList<>();
        try {
            OERResponse exchangeRates = objectMapper.readValue(inputStream, OERResponse.class);
            if (exchangeRates.getRates() != null && exchangeRates.getBase() != null) {
                for (String currency : exchangeRates.getRates().keySet()) {
                    CurrencyUnit currencyUnit = getCurrencyUnit(currency);
                    if (currencyUnit != null) {
                        result.add(new ExchangeRate(
                                exchangeRates.getRates().get(currency),
                                CurrencyUnit.of(exchangeRates.getBase()),
                                CurrencyUnit.of(currency)));
                    }
                }
            }
        } catch (IOException ex) {
            throw new ExchangeRateParserException(ex);
        }
        return result;
    }

    private CurrencyUnit getCurrencyUnit(final String currency) {
        if (currency == null) {
            return null;
        }
        try {
            return CurrencyUnit.of(currency);
        } catch (IllegalCurrencyException ex) {
            // Joda Money does not seem to support Bitcoin (its old code BTC and new code XTB.
            // Also, for some currencies, the OpenExchangeRates service returns old codes (like BYR for
            // belorussian ruble, which should be BYN now), so there are multiple conflicts with Joda Money.
            // As for now, let's ignore them.
            logger.warn("Currency " + currency + " was ignored because it's currently missing in joda.money.");
            return null;
        }
    }
}
