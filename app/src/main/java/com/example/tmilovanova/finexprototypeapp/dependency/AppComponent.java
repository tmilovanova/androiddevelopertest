package com.example.tmilovanova.finexprototypeapp.dependency;

import com.example.tmilovanova.finexprototypeapp.ui.ExchangeActivity;
import dagger.Component;

import javax.inject.Singleton;

@Component(modules = DependencyModule.class)
@Singleton
public interface AppComponent {

    void inject(ExchangeActivity exchangeActivity);
}
