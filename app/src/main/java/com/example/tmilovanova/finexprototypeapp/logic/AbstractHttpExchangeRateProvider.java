package com.example.tmilovanova.finexprototypeapp.logic;

import android.support.annotation.Nullable;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Base class for providers taking the rates via an HTTP call and parsing the from the request body.
 */
public abstract class AbstractHttpExchangeRateProvider implements ExchangeRateProvider {

    private static final String RETRIEVAL_ERROR_MESSAGE_TEMPLATE = "Exchange rate retrieval failed with HTTP response code %d, reason: %s";

    private final OkHttpClient httpClient;
    private final ExchangeRateParser exchangeRateParser;

    @Getter(AccessLevel.PROTECTED)
    private final SimpleLogger logger;

    /**
     * @param httpClient the {@link OkHttpClient} instance to be used for data requests
     * @param exchangeRateParser the parser instance to read the rates from the response
     * @param logger the logger instance
     */
    protected AbstractHttpExchangeRateProvider(OkHttpClient httpClient, ExchangeRateParser exchangeRateParser,
            SimpleLogger logger) {
        this.httpClient = httpClient;
        this.exchangeRateParser = exchangeRateParser;
        this.logger = logger;
    }

    @Override
    public void getAsync(@NonNull final ExchangeRateCallback callback) {
        Request request = new Request.Builder()
                .url(getUrl())
                .build();

        logger.debug(getClass().getSimpleName() + ": starting rates retrieval, url is " + getUrl());

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException ex) {
                logException(ex);
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    reportSuccess(response, callback);
                } else {
                    logHttpCallFailure(response);
                    callback.onResult(Collections.<ExchangeRate>emptyList());
                }
                response.body().close();
            }
        });
    }

    /**
     * This method should be triggered after the HTTP call to retrieve the exchange rates
     * from the response body.
     *
     * @param response the HTTP response to retrieve the rate info from
     * @param externalCallback the client callback object. Will be triggered once when the full list of
     * available rates will be completed. If an error happened during retrieval, should
     * return an empty list.
     */
    protected void reportSuccess(Response response, ExchangeRateCallback externalCallback) {
        try {
            List<ExchangeRate> parsedRates = exchangeRateParser.parse(response.body().byteStream());
            List<ExchangeRate> postProcessedRates = postProcessRates(parsedRates);
            externalCallback.onResult(postProcessedRates);
            logger.debug(getClass().getSimpleName() + ": rate retrieval is finished successfully.");
        } catch (ExchangeRateParserException ex) {
            logException(ex);
        }
    }

    protected List<ExchangeRate> postProcessRates(final List<ExchangeRate> parsedRates) {
        return parsedRates;
    }

    /**
     * This method should be triggered when an error happened at any stage of the exchange rate retrieval.
     * The client should know only about the specific rates (un)availability; all related errors and debug info
     * should be, however, printed to the log.
     *
     * @param response the HTTP response to retrieve the error info from
     */
    protected void logHttpCallFailure(Response response) {
        logger.error(getRetrievalErrorMessage(response.code(), null));
    }

    private void logException(Exception exception) {
        logger.error(getClass().getName() + " encountered an error while retrieving exchange rates: ", exception);
    }

    /**
     * Provides a useful internal error message that happened during the exchange rate retrieval via some API.
     *
     * @param httpCode the HTTP code of the response
     * @param reason the reason of the error, if provided. If not, will be set to "unknown".
     * @return the appropriate error message
     */
    protected String getRetrievalErrorMessage(int httpCode, @Nullable String reason) {
        return String.format(Locale.ENGLISH, RETRIEVAL_ERROR_MESSAGE_TEMPLATE, httpCode, (reason == null ? "unknown" : reason));
    }

    /**
     * The endpoing URI to get the exchange rate payload from.
     */
    protected abstract String getUrl();
}
