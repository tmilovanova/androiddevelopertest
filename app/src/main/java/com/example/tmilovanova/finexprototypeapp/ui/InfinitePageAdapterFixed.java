package com.example.tmilovanova.finexprototypeapp.ui;

import android.support.v4.view.PagerAdapter;
import android.view.ViewGroup;
import com.antonyt.infiniteviewpager.InfinitePagerAdapter;
import com.antonyt.infiniteviewpager.InfiniteViewPager;

public class InfinitePageAdapterFixed extends InfinitePagerAdapter {

    private final InfiniteViewPager viewPager;

    public InfinitePageAdapterFixed(InfiniteViewPager viewPager, final PagerAdapter adapter) {
        super(adapter);
        this.viewPager = viewPager;
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        super.destroyItem(container, position, object);
        /* If there are less than 4 pages in the pager, then the InfiniteViewPager,
         when scrolling left, destroys the same items after it has "instantiated" them.
         Another approach at fixing this problem would be to just double the data, which
         seems to work equally well for this particular application case
         (see com.example.tmilovanova.finexprototypeapp.dependency.DependencyModule#EXCHANGE_CURRENCIES),
         but this specific approach seems a little less hacky than doubling the data.*/
        super.instantiateItem(container, position % getRealCount());
    }
}
