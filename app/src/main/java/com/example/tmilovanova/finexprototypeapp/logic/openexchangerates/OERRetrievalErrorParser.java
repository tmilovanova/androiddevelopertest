package com.example.tmilovanova.finexprototypeapp.logic.openexchangerates;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;

public class OERRetrievalErrorParser {
    private final ObjectMapper objectMapper;

    @Inject
    public OERRetrievalErrorParser() {
        this.objectMapper = new ObjectMapper();
    }

    OERErrorResponse parse(InputStream jsonData) throws IOException {
        return objectMapper.readValue(jsonData, OERErrorResponse.class);
    }
}
