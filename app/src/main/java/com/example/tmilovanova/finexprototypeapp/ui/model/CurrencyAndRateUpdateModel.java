package com.example.tmilovanova.finexprototypeapp.ui.model;

import android.content.res.Resources;
import com.example.tmilovanova.finexprototypeapp.R;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import com.example.tmilovanova.finexprototypeapp.ui.ExchangeActivity;
import lombok.Getter;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Serves as Model for the app's {@link ExchangeActivity} and its fragments.
 * The model is based on the rule that at a given moment a pair of currencies become "active":
 * the "convert from" one and the "convert to" one. They can change in 2 cases:
 * - the user scrolls the top / bottom view pagers changing one of the currencies;
 * - the user focuses the top / bottom view pagers to change the direction of the conversion.
 * Also one more part the user can change is the amount of the "from" currency he/she would like
 * to convert to the "to" currency.
 * This model tracks these changes and triggers appropriate cascade changes to other parts of the View.
 * Since these events come from fragments holding amount input boxes and currency codes the user interacts with,
 * this class serves as a bus for such events, notifying all listeners (other fragments) about
 * the changes in one of them so they could change their state appropriately.
 */
public class CurrencyAndRateUpdateModel {

    public static final String SUPPORTED_CURRENCIES = "SupportedCurrencies";
    public static final String DEFAULT_FROM_CURRENCY_INDEX = "DefaultFromCurrencyIndex";
    public static final String DEFAULT_TO_CURRENCY_INDEX = "DefaultToCurrencyIndex";

    private static final String EMPTY_LABEL = "";

    private List<DisplayedRatesUpdateEventListener> displayedRatesUpdateEventListeners;
    private List<ValueChangeListener> valueChangeListeners;
    private ExchangeDirection exchangeDirection;

    private final Resources resources;
    @Getter
    private final List<String> supportedCurrencies;
    @Getter
    private final int defaultFromCurrencyIndex;
    @Getter
    private final int defaultToCurrencyIndex;
    private List<ExchangeRate> currentExchangeRates;
    @Getter
    private String fromCurrencyInFocus;
    @Getter
    private String toCurrencyInFocus;

    /**
     * @param resources an instance of {@link Resources} to resolve application string resources
     * @param supportedCurrencies a list of currencies supported by this application
     * @param defaultFromCurrencyIndex the initial value of the "from" currency
     * @param defaultToCurrencyIndex the initial value of the "to" currency
     */
    @Inject
    public CurrencyAndRateUpdateModel(Resources resources, @Named(SUPPORTED_CURRENCIES) List<String> supportedCurrencies,
            @Named(DEFAULT_FROM_CURRENCY_INDEX) int defaultFromCurrencyIndex,
            @Named(DEFAULT_TO_CURRENCY_INDEX) int defaultToCurrencyIndex) {
        this.resources = resources;
        this.supportedCurrencies = supportedCurrencies;
        this.defaultFromCurrencyIndex = defaultFromCurrencyIndex;
        this.defaultToCurrencyIndex = defaultToCurrencyIndex;
        this.fromCurrencyInFocus = supportedCurrencies.get(defaultFromCurrencyIndex);
        this.toCurrencyInFocus = supportedCurrencies.get(defaultToCurrencyIndex);
        this.displayedRatesUpdateEventListeners = new ArrayList<>();
        this.valueChangeListeners = new ArrayList<>();
        // * 2 is for inverted rates
        this.currentExchangeRates = new ArrayList<>(supportedCurrencies.size() * 2);
        this.exchangeDirection = ExchangeDirection.DIRECT;
    }

    /**
     * Subscribes the {@link DisplayedRatesUpdateEventListener} listener instance to events of updating the currency rate to
     * be displayed. Involves updates on rate changes and conversion direction changes.
     */
    public void addDisplayedRatesUpdateEventListener(DisplayedRatesUpdateEventListener listener) {
        if (listener != null) {
            displayedRatesUpdateEventListeners.add(listener);
        }
    }

    /**
     * Performs the necessary model changes in the event of a "from" or "to" currency changes.
     * Such a change can occur when the user is scrolling one of the {@code ViewPager} widgets
     * to switch currencies.
     *
     * @param newCurrency the new currency value
     * @param position the position of the widget where the change has occurred.
     */
    public void onCurrencyUpdate(final String newCurrency, ExchangeInputPosition position) {
        if (position.getCorrespondingDirection() == exchangeDirection) {
            fromCurrencyInFocus = newCurrency;
        } else {
            toCurrencyInFocus = newCurrency;
        }
        publishNewValue(Money.of(CurrencyUnit.of(newCurrency), BigDecimal.ZERO));
        refreshDisplayedRates();
    }

    private void onCurrencyFlipUpdate() {
        String temp = fromCurrencyInFocus;
        fromCurrencyInFocus = toCurrencyInFocus;
        toCurrencyInFocus = temp;
        refreshDisplayedRates();
    }

    /**
     * Performs the necessary model changes in the event of user input focus changed (if needed).
     * Such a change could mean the event of switching between the currency widget positions
     * and with this effectively changing the conversion direction (for example, EUR->USD
     * will become USD->EUR).
     * However, this method allows for false alarm triggers, as it checks if the position
     * really changed first.
     */
    public void onRateInputFocusObtained(ExchangeInputPosition inputPosition) {
        if (inputPosition != null && inputPositionChanged(inputPosition)) {
            exchangeDirection = exchangeDirection.flip();
            onCurrencyFlipUpdate();
        }
    }

    private boolean inputPositionChanged(final ExchangeInputPosition position) {
        return exchangeDirection != position.getCorrespondingDirection();
    }

    /**
     * This method adds a listener for the user amount value updates.
     *
     * @see #publishNewValue(Money)
     */
    public void addValueChangeListener(ValueChangeListener listener) {
        valueChangeListeners.add(listener);
    }

    /**
     * This method removes a listener for the user amount value updates, if the listener was not
     * subscribed to these events, nothing happens.
     * @see #addValueChangeListener(ValueChangeListener), {@link #publishNewValue(Money)}
     */
    public void removeValueChangeListener(ValueChangeListener listener) {
        valueChangeListeners.remove(listener);
    }

    /**
     * When the user enters the amount in a specific currency, some other widgets may need to know about
     * this to adjust their visual information appropriately.
     * When such an event happens, the money entered by the user are converted to the current "to" currency
     * using currently available rates and all the subscribed widgets receive an update. The widget that corresponds
     * to the published value (i.e. the one that converts FROM the "to" currency to the currency used by
     * the passed {@code Money} instance should react to the update.
     * @param money
     */
    public void publishNewValue(Money money) {
        for (ExchangeRate rate : currentExchangeRates) {
            if (rate.getFrom().equals(money.getCurrencyUnit())) {
                if (rate.getTo().getCode().equals(toCurrencyInFocus)) {
                    // the fragment to be changed is going to have an inverted
                    // position from the currently active one
                    ExchangeDirection direction = exchangeDirection.flip();
                    for (ValueChangeListener listener : valueChangeListeners) {
                        listener.handleNewValue(rate.convert(money),
                                ExchangeInputPosition.fromExchangeDirection(direction));
                    }
                    break;
                }
            }
        }

    }

    private void refreshDisplayedRates() {
        String newLabel = EMPTY_LABEL;
        if (fromCurrencyInFocus != null && toCurrencyInFocus != null &&
                !fromCurrencyInFocus.equals(toCurrencyInFocus)) {
            ExchangeRate rate = getExchangeRate(fromCurrencyInFocus, toCurrencyInFocus);
            if (rate == null) {
                newLabel = resources.getString(R.string.unavailable_rate);
            } else {
                newLabel = resources.getString(R.string.current_rate, CurrencyUnit.of(fromCurrencyInFocus).getSymbol(),
                        CurrencyUnit.of(toCurrencyInFocus).getSymbol(), rate.getRate().toString());
            }
        }
        for (DisplayedRatesUpdateEventListener listener : displayedRatesUpdateEventListeners) {
            listener.onDisplayedRateRefreshed(newLabel);
        }
    }

    private ExchangeRate getExchangeRate(final String fromCurrencyInFocus, final String toCurrencyInFocus) {
        for (ExchangeRate rate : currentExchangeRates) {
            if (rate.getFrom().getCode().equals(fromCurrencyInFocus) &&
                    rate.getTo().getCode().equals(toCurrencyInFocus)) {
                return rate;
            }
        }
        return null;
    }

    /**
     * Performs the necessary model changes on the event of the updated rates received
     * from some external source.
     * @param newExchangeRates
     *      the new rate values
     */
    public void onExchangeRatesUpdate(final List<ExchangeRate> newExchangeRates) {
        currentExchangeRates.clear();
        for (ExchangeRate rate : newExchangeRates) {
            if (supportedCurrencies.contains(rate.getFrom().getCode()) &&
                    supportedCurrencies.contains(rate.getTo().getCode())) {
                currentExchangeRates.add(rate);
            }
        }
        refreshDisplayedRates();
    }

    /**
     * Interface for receiving notifications about value change in one of the existing
     * currency/amount input widgets.
     */
    public interface ValueChangeListener {

        /**
         * This method should react appropriately to the input amount changed
         * in a specific currency and exchange direction.
         *
         * @param money a {@link Money} instance that holds information about the changed amount
         * and its currency
         * @param position the position of the widget the change should be related to (this should be
         * opposite to the position of the widget where the change initially occurred).
         */
        void handleNewValue(Money money, ExchangeInputPosition position);
    }
}
