package com.example.tmilovanova.finexprototypeapp.logic.openexchangerates;

import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateCallback;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import org.joda.money.CurrencyUnit;
import org.junit.Test;
import org.mockito.Mockito;
import utils.OkHttpClientTester;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OERExchangeRateProviderTest {

    private static final String FAKE_APP_ID = "FAKE_APP_ID";
    private static final String FAKE_BASE = "USD";

    @Test
    public void testRateFromBaseCurrencyToItselfIsRemoved() throws Exception {
        List<ExchangeRate> fakeRates = new ArrayList<>(2);
        final ExchangeRate firstItem = new ExchangeRate(BigDecimal.valueOf(2.0), CurrencyUnit.USD, CurrencyUnit.EUR);
        fakeRates.add(firstItem);
        final ExchangeRate secondItem = new ExchangeRate(BigDecimal.valueOf(1.0), CurrencyUnit.USD, CurrencyUnit.USD);
        fakeRates.add(secondItem);

        OkHttpClientTester okHttpClientTester = new OkHttpClientTester.Builder()
                .responseBody("")
                .responseCode(200)
                .build();

        ExchangeRateParser mockParser = mock(ExchangeRateParser.class);
        when(mockParser.parse(any(InputStream.class))).thenReturn(fakeRates);

        OERExchangeRateProvider testInstance = new OERExchangeRateProvider(okHttpClientTester.getMockClient(),
                mockParser, mock(OERRetrievalErrorParser.class), FAKE_APP_ID, FAKE_BASE, mock(SimpleLogger.class));
        testInstance.getAsync(new ExchangeRateCallback() {
            @Override
            public void onResult(final List<ExchangeRate> exchangeRate) {
                assertTrue(exchangeRate.contains(firstItem));
                assertFalse(exchangeRate.contains(secondItem));
            }
        });
    }

    @Test
    public void testHttpErrorsAreParsedAndLogged() throws Exception {
        OERRetrievalErrorParser mockErrorParser = mock(OERRetrievalErrorParser.class);
        OERErrorResponse fakeErrorResponse = getFakeErrorResponse();
        when(mockErrorParser.parse(any(InputStream.class))).thenReturn(fakeErrorResponse);

        OkHttpClientTester okHttpClientTester = new OkHttpClientTester.Builder()
                .responseBody("")
                .responseCode(403)
                .build();

        SimpleLogger mockLogger = mock(SimpleLogger.class);
        OERExchangeRateProvider testInstance = new OERExchangeRateProvider(okHttpClientTester.getMockClient(), mock(ExchangeRateParser.class),
                mockErrorParser, FAKE_APP_ID, FAKE_BASE, mockLogger);
        ExchangeRateCallback externalCallback = mock(ExchangeRateCallback.class);
        testInstance.getAsync(externalCallback);

        okHttpClientTester.triggerCallbackAndVerify();
        verify(mockLogger).error(Mockito.contains(fakeErrorResponse.getDescription()));
        verify(externalCallback).onResult(eq(Collections.<ExchangeRate>emptyList()));
    }

    private OERErrorResponse getFakeErrorResponse() {
        return new OERErrorResponse(true, 403, "Access denied", "You are not properly authorized");
    }

}