package com.example.tmilovanova.finexprototypeapp.logic.eurofx;

import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import org.joda.money.CurrencyUnit;
import org.junit.Test;
import utils.ParserTestUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EuroFxXmlExchangeRateParserTest {

    private EuroFxXmlExchangeRateParser testInstance = new EuroFxXmlExchangeRateParser();

    @Test
    public void testParsingListOf5CorrectItemsShouldSucceed() {
        List<ExchangeRate> expected = new ArrayList<>(6);
        expected.add(new ExchangeRate(BigDecimal.valueOf(10609, 4), CurrencyUnit.EUR, CurrencyUnit.USD));
        expected.add(new ExchangeRate(BigDecimal.valueOf(11904, 2), CurrencyUnit.EUR, CurrencyUnit.JPY));
        expected.add(new ExchangeRate(BigDecimal.valueOf(19558, 4), CurrencyUnit.EUR, CurrencyUnit.of("BGN")));
        expected.add(new ExchangeRate(BigDecimal.valueOf(27021, 3), CurrencyUnit.EUR, CurrencyUnit.of("CZK")));
        expected.add(new ExchangeRate(BigDecimal.valueOf(74344, 4), CurrencyUnit.EUR, CurrencyUnit.of("DKK")));

        test("5_items", expected);
    }

    @Test
    public void testParsingInputWithNoRatesShouldReturnEmptyList() {
        test("empty", Collections.<ExchangeRate> emptyList());
    }

    @Test
    public void testParsingShouldIgnoreRatesWithIncorrectOrMissingAttributes() {
        List<ExchangeRate> expected = new ArrayList<>(1);
        expected.add(new ExchangeRate(new BigDecimal(BigInteger.TEN, 1), CurrencyUnit.EUR, CurrencyUnit.USD));

        test("attrs", expected);
    }

    private void test(String inputFileId, List<ExchangeRate> expected) {
        ParserTestUtils.test(getTestFileName(inputFileId), expected, testInstance);
    }

    private String getTestFileName(String id) {
        return "euro_fx_test_rates_" + id + ".xml";
    }
}