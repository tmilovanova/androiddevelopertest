package com.example.tmilovanova.finexprototypeapp.logic.openexchangerates;

import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import org.joda.money.CurrencyUnit;
import org.junit.Test;
import utils.ParserTestUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class OERExchangeRateParserTest {

    private SimpleLogger mockLogger = mock(SimpleLogger.class);
    private OERExchangeRateParser testInstance = new OERExchangeRateParser(mockLogger);

    @Test
    public void testParsingListOf5CorrectItemsShouldSucceed() {
        List<ExchangeRate> expected = new ArrayList<>(5);
        expected.add(new ExchangeRate(BigDecimal.valueOf(3672695, 6), CurrencyUnit.USD, CurrencyUnit.of("AED")));
        expected.add(new ExchangeRate(BigDecimal.valueOf(66805, 3), CurrencyUnit.USD, CurrencyUnit.of("AFN")));
        expected.add(new ExchangeRate(BigDecimal.valueOf(127695574, 6), CurrencyUnit.USD, CurrencyUnit.of("ALL")));
        expected.add(new ExchangeRate(BigDecimal.valueOf(489181052, 6), CurrencyUnit.USD, CurrencyUnit.of("AMD")));
        expected.add(new ExchangeRate(BigDecimal.valueOf(1788035, 6), CurrencyUnit.USD, CurrencyUnit.of("ANG")));

        test("5_items", expected);
    }

    @Test
    public void testParsingEmptyListShouldReturnEmptyList() {
        test("empty", Collections.<ExchangeRate> emptyList());
    }

    @Test
    public void testParsingWithNoBaseShouldReturnEmptyList() {
        test("no_base", Collections.<ExchangeRate> emptyList());
    }

    @Test
    public void testParsingWithNoRatesShouldReturnEmptyList() {
        test("no_rates", Collections.<ExchangeRate> emptyList());
    }

    @Test
    public void testParsingIgnoresCurrenciesUnsupportedByJodaMoney() {
        test("unsupported_currency", Collections.<ExchangeRate>emptyList());
        verify(mockLogger, times(3)).warn(anyString());
    }

    private void test(String inputFileId, List<ExchangeRate> expected) {
        ParserTestUtils.test("oer_test_rates_" + inputFileId + ".json", expected, testInstance);
    }

}