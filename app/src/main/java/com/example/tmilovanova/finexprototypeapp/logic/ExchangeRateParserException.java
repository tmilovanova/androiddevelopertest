package com.example.tmilovanova.finexprototypeapp.logic;

/**
 * A generic exception wrapper for {@link ExchangeRateParser} class.
 */
public class ExchangeRateParserException extends Exception {

    private static final String PARSER_EXCEPTION_MESSAGE = "There was a problem when trying to parse exchange rates";

    public ExchangeRateParserException(Exception cause) {
        super(PARSER_EXCEPTION_MESSAGE, cause);
    }
}
