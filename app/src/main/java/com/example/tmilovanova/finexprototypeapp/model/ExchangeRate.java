package com.example.tmilovanova.finexprototypeapp.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Represents an Exchange Rate object that holds information about financial exchange
 * rate from one currency to another.
 */
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ExchangeRate {

    @Getter
    private final BigDecimal rate;

    @Getter
    private final CurrencyUnit from;

    @Getter
    private final CurrencyUnit to;

    /**
     * Converts the given amount of money in the {@code from} currency to
     * the amount in the {@code to} currency using the current {@code rate}.
     * @param input
     *      the instance of {@link Money} to convert from
     * @return
     *      the instance of {@link Money} that represents the conversion result
     * @throws
     *      IllegalArgumentException if the currency of the input {@code Money} instance
     *          does not match the {@code from} currency specified in the current {@code ExchangeRate} object.
     */
    public Money convert(@NonNull Money input) {
        if (!from.equals(input.getCurrencyUnit())) {
            throw new IllegalArgumentException("Currency unit does not match: should be " + from);
        }
        return input.convertedTo(to, rate, RoundingMode.HALF_UP);
    }

    /**
     * Produces the inverse exchange rate as {@code 1/rate} for {@code to}->{@code from} conversion.
     * The scale of the result will be the same as the scale of the initial rate.
     * The result will be rounded similar to the {@link BigDecimal#ROUND_HALF_UP} strategy.
     *
     * @return
     */
    public ExchangeRate inverted() {
        BigDecimal invertedRate = BigDecimal.ONE.divide(rate, rate.scale(), BigDecimal.ROUND_HALF_UP);
        return new ExchangeRate(invertedRate, to, from);
    }
}
