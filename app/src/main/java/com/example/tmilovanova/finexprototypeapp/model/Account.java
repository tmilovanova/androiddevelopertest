package com.example.tmilovanova.finexprototypeapp.model;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import javax.inject.Inject;
import java.math.BigDecimal;

public class Account {

    @Inject
    public Account() {
    }

    public Money getBalance(CurrencyUnit currencyUnit) {
        return Money.of(currencyUnit, BigDecimal.ZERO);
    }
}
