package utils;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OkHttpClientTester {

    private OkHttpClient mockClient;
    private Call mockCall;
    private Response fakeResponse;
    private String responseBody;
    private int responseCode;

    public static class Builder {

        private OkHttpClientTester okHttpClientTester = new OkHttpClientTester();

        public Builder responseCode(int code) {
            okHttpClientTester.responseCode = code;
            return this;
        }

        public Builder responseBody(String body) {
            okHttpClientTester.responseBody = body;
            return this;
        }

        public OkHttpClientTester build() {
            okHttpClientTester.setUp();
            return okHttpClientTester;
        }
    }

    private void setUp() {
        mockClient = mock(OkHttpClient.class);
        mockCall = mock(Call.class);
        when(mockClient.newCall(any(Request.class))).thenReturn(mockCall);
        ResponseBody mockResponseBody = mock(ResponseBody.class);
        BufferedSource mockBufferedSource = mock(BufferedSource.class);
        InputStream memoryStream = new ByteArrayInputStream(responseBody.getBytes(Charset.forName("UTF-8")));
        when(mockBufferedSource.inputStream()).thenReturn(memoryStream);
        when(mockResponseBody.source()).thenReturn(mockBufferedSource);
        fakeResponse = new Response.Builder()
                .body(mockResponseBody)
                .protocol(Protocol.HTTP_1_0)
                .request(new Request.Builder().url("http://google.com").build())
                .code(responseCode)
                .build();
    }

    public OkHttpClient getMockClient() {
        return mockClient;
    }

    public void triggerCallbackAndVerify() throws IOException {
        ArgumentCaptor<Callback> callbackCaptor = ArgumentCaptor.forClass(Callback.class);
        Mockito.verify(mockCall).enqueue(callbackCaptor.capture());
        callbackCaptor.getValue().onResponse(mockCall, fakeResponse);
    }

}
