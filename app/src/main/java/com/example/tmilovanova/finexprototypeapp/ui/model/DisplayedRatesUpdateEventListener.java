package com.example.tmilovanova.finexprototypeapp.ui.model;

public interface DisplayedRatesUpdateEventListener {

    void onDisplayedRateRefreshed(String newRateToDisplay);
}
