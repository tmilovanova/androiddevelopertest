package com.example.tmilovanova.finexprototypeapp.logic;

import android.content.Context;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;

import javax.inject.Inject;
import javax.inject.Named;

public class ExchangeRateLoaderFactory {

    private final ExchangeRateProvider provider;
    private final long repeatPeriodMillis;
    private final SimpleLogger logger;
    public static final String LOADER_REPEAT_PERIOD_MILLIS = "loaderRepeatPeriodMillis";

    @Inject
    public ExchangeRateLoaderFactory(ExchangeRateProvider provider,
            @Named(LOADER_REPEAT_PERIOD_MILLIS) long repeatPeriodMillis,
            SimpleLogger logger) {
        this.provider = provider;
        this.repeatPeriodMillis = repeatPeriodMillis;
        this.logger = logger;
    }

    public ExchangeRateLoader createLoader(Context context) {
        return new ExchangeRateLoader(context, provider,
                repeatPeriodMillis, logger);
    }
}
