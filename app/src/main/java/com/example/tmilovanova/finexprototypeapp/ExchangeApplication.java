package com.example.tmilovanova.finexprototypeapp;

import android.app.Application;
import android.content.Context;
import com.example.tmilovanova.finexprototypeapp.dependency.AppComponent;
import com.example.tmilovanova.finexprototypeapp.dependency.DaggerAppComponent;

public class ExchangeApplication extends Application {

    private static AppComponent component;
    private static Context applicationContext;

    public ExchangeApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().build();
        applicationContext = getApplicationContext();
    }

    public static AppComponent getComponent() {
        return component;
    }

    public static Context getContext() {
        return applicationContext;
    }
}
