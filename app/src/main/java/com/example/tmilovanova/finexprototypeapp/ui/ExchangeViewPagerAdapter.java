package com.example.tmilovanova.finexprototypeapp.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.example.tmilovanova.finexprototypeapp.model.Account;
import com.example.tmilovanova.finexprototypeapp.ui.model.ExchangeInputPosition;
import org.joda.money.CurrencyUnit;

import javax.inject.Provider;
import java.util.List;

class ExchangeViewPagerAdapter extends FragmentPagerAdapter {

    private final List<String> currencies;
    private final Provider<Account> accountProvider;
    private final ExchangeInputPosition widgetPosition;

    ExchangeViewPagerAdapter(FragmentManager fragmentManager,
            List<String> currencies, Provider<Account> accountProvider, ExchangeInputPosition widgetPosition) {
        super(fragmentManager);
        this.currencies = currencies;
        this.accountProvider = accountProvider;
        this.widgetPosition = widgetPosition;
    }

    @Override
    public Fragment getItem(final int position) {
        final String currencyCode = currencies.get(position);
        Account account = accountProvider.get();
        CurrencyUnit currency = CurrencyUnit.of(currencyCode);
        final String currentBalance = currency.getSymbol()
                + account.getBalance(currency).getAmount().toString();
        return ExchangeViewFragment.newInstance(widgetPosition, currencyCode, currentBalance);
    }

    @Override
    public int getCount() {
        return currencies.size();
    }

    public String getCurrencyByPosition(int position) {
        return currencies.get(position);
    }
}
