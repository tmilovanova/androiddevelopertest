package com.example.tmilovanova.finexprototypeapp.ui;

import android.text.InputFilter;
import android.text.Spanned;
import org.joda.money.CurrencyUnit;

/*
 *  The {@link InputFilter} implementation that prevents the user from entering more
 *  decimal digits than expected by the currency.
 */
class DecimalPlacesFilter implements InputFilter {

    private final String currency;

    DecimalPlacesFilter(String currency) {
        this.currency = currency;
    }

    @Override
    public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart, final int dend) {
        // if it is a deletion, allow it - it is safe
        if (start == end) {
            return null;
        }
        // TODO !!! Change this if you start to use localized output (meaning different delimiters)
        int pointIndex = dest.toString().indexOf('.');
        if (pointIndex != -1 &&
                dest.length() - pointIndex >= CurrencyUnit.of(currency).getDecimalPlaces() + 1 &&
                dstart > pointIndex) {
            return "";
        }
        return null;
    }
}
