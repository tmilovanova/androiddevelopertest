package com.example.tmilovanova.finexprototypeapp.logic.eurofx;

import com.example.tmilovanova.finexprototypeapp.logic.AbstractHttpExchangeRateProvider;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.logging.SimpleLogger;
import okhttp3.OkHttpClient;

import javax.inject.Inject;

/**
 * Provides EUR-based rates taken from the European Central Bank site.
 */
public class EuroFxExchangeRateProvider extends AbstractHttpExchangeRateProvider {

    private static final String EURO_FX_URL = "http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml";

    @Inject
    public EuroFxExchangeRateProvider(OkHttpClient httpClient, ExchangeRateParser exchangeRateParser,
            SimpleLogger logger) {
        super(httpClient, exchangeRateParser, logger);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getUrl() {
        return EURO_FX_URL;
    }

}
