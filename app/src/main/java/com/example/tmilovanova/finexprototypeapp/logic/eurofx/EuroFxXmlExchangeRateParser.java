package com.example.tmilovanova.finexprototypeapp.logic.eurofx;

import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParser;
import com.example.tmilovanova.finexprototypeapp.logic.ExchangeRateParserException;
import com.example.tmilovanova.finexprototypeapp.model.ExchangeRate;
import org.joda.money.CurrencyUnit;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses the exchange rates corresponding to the XML scheme applied to
 * current rate XML responses on European Central bank site as of February 2017.
 *
 * @see <a href="Example XML">http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml</a>
 */
public class EuroFxXmlExchangeRateParser implements ExchangeRateParser {

    private static final CurrencyUnit BASE_CURRENCY = CurrencyUnit.EUR;

    @Inject
    public EuroFxXmlExchangeRateParser() {
    }

    /**
     * Parses the XML input and returns a list of current EUR exchange rates listed there.
     * The implementation relies on the presence of the following elements in the XML document:
     * <pre>
     * {@code
     * <Cube currency="XXX" rate="N" />
     * }
     * </pre>
     * <p>
     * The tag name should match and both attributes should be present, otherwise the element will be ignored.
     * Nesting and placement of those tags in the document hierarchy is not checked.
     *
     * @param inputStream the JSON byte input stream to be parsed
     * @return the result list of the exchange rates
     * @throws ExchangeRateParserException on any exception withing the internal XML parser which will
     * be included as the cause.
     */
    @Override
    public List<ExchangeRate> parse(final InputStream inputStream) throws ExchangeRateParserException {
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            XMLReader xmlReader = saxParserFactory.newSAXParser().getXMLReader();

            CurrencySaxParserHandler saxParserHandler = new CurrencySaxParserHandler(BASE_CURRENCY);
            xmlReader.setContentHandler(saxParserHandler);

            InputSource inputSource = new InputSource();
            inputSource.setByteStream(inputStream);

            xmlReader.parse(inputSource);

            return saxParserHandler.exchangeRates;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new ExchangeRateParserException(ex);
        }

    }

    private static class CurrencySaxParserHandler extends DefaultHandler {

        private static final String TAG_CUBE = "Cube";
        private static final String ATTRIBUTE_CURRENCY = "currency";
        private static final String ATTRIBUTE_RATE = "rate";

        private final CurrencyUnit baseCurrency;
        private final List<ExchangeRate> exchangeRates = new ArrayList<>();

        CurrencySaxParserHandler(final CurrencyUnit baseCurrency) {
            this.baseCurrency = baseCurrency;
        }

        @Override
        public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
            if (TAG_CUBE.equals(qName)) {
                final String currencyCode = attributes.getValue(ATTRIBUTE_CURRENCY);
                final String rate = attributes.getValue(ATTRIBUTE_RATE);
                if (currencyCode != null && rate != null) {
                    final BigDecimal decRate = new BigDecimal(rate);
                    final CurrencyUnit currencyUnit = CurrencyUnit.of(currencyCode);
                    exchangeRates.add(new ExchangeRate(decRate, baseCurrency, currencyUnit));
                }
            }
        }
    }
}
